
from setuptools import setup

setup(name='pytoolcore',
      version='0.2',
      description='Various element building a command-line interface app',
      url='https://gitlab.com/0x6578656376652829/Pytoolcore',
      author='Danakane',
      author_email='dal.li@outlook.com',
      license='MIT',
      packages=['pytoolcore'],
      zip_safe=False)
